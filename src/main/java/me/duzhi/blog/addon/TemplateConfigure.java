package me.duzhi.blog.addon;

import com.jfinal.render.FreeMarkerRender;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.IOException;
import java.util.Set;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 19, 2017
 */

public class TemplateConfigure {
    private static TemplateConfigure templateConfigure = new TemplateConfigure();
    private  StringTemplateLoader stringLoader;
    private  Configuration cfg = new Configuration();

    public TemplateConfigure() {
        stringLoader = new StringTemplateLoader();
        cfg.setTemplateLoader(stringLoader);
        Set<String> set = FreeMarkerRender.getConfiguration().getSharedVariableNames();
        for (String o : set) {
            cfg.setSharedVariable(o,FreeMarkerRender.getConfiguration().getSharedVariable(o));
        }
    }

    public static TemplateConfigure me(){
        return templateConfigure;
    }

    public StringTemplateLoader getTemplateLoader() {
        return stringLoader;
    }



    public Configuration getConfiguration() {
        return cfg;
    }


    public void putTemplate(String id, String html) {
        stringLoader.putTemplate(id,html);
    }

    public Template getTemplate(String id, String s) throws IOException {
        return cfg.getTemplate(id);
    }
}
